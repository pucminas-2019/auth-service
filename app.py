import os
import pyrebase
from flask import Flask, jsonify, request
from flask_restful import Resource, Api, reqparse
from requests.exceptions import HTTPError
from flask_cors import CORS

app = Flask(__name__)
CORS(app)
api = Api(app, prefix='/services/auth')

config = {
	"apiKey": os.environ.get('API_KEY'), 
    "authDomain": os.environ.get('AUTH_DOMAIN'), 
    "databaseURL": os.environ.get('DATABASE_URL'), 
    "projectId": os.environ.get('PROJECT_ID'),
    "storageBucket": os.environ.get('STORAGE_BUCKET'),
    "messagingSenderId": os.environ.get('MESSAGING_SENDER_ID')
}

firebase = pyrebase.initialize_app(config)

auth = firebase.auth()

class Authenticate(Resource):
    def post(self):
        args = request.get_json(force=True) 
        username = args['username']
        password = args['password']
        resp = None
        
        try:
            user = auth.sign_in_with_email_and_password(username, password)
            user_token = user['idToken']
            user_id = user['localId']
            resp = jsonify({'token':user_token, 'user_id': user_id})
        except HTTPError:
            resp = jsonify({'message': 'Invalid Credentials'})
            resp.status_code = 401
        
        return resp 


class ValidateToken(Resource):
    def post(self):
        
        args = request.get_json(force=True)
        
        try:
            access_token = args['access_token']
        except KeyError:
            return jsonify({'message': 'access_token are missing.'})
    
        try:
            account_info = auth.get_account_info(access_token)
        except HTTPError as e:
            resp = jsonify({'message': 'Invalid Token'})
            resp.status_code = 401
            return resp 

        return jsonify(account_info)


api.add_resource(Authenticate, '/authenticate')
api.add_resource(ValidateToken, '/validate-token')

if __name__ == "__main__":
	app.run(debug=True, host='0.0.0.0')
